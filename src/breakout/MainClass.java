package breakout;

import java.io.File;

import breakout.view.MainWindow;
import javafx.application.Application;

/**
 * Main class of the application.
 */
public final class MainClass {

    private MainClass() {
    }

    /**
     * Launch application.
     * 
     * @param args
     *            arguments
     */
    public static void main(final String[] args) {

        // Creates .breakout folder in user home
        final File breakoutDir = new File(System.getProperty("user.home") + File.separator + ".breakout");
        if (!breakoutDir.exists()) {
            breakoutDir.mkdir();
        }
        
        Application.launch(MainWindow.class, args);

    }

}
